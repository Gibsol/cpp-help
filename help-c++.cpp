#include <iostream>
using namespace std;

int main() {

  char choice;
  char quit;

  for(;;) {
  do {
    cout << "  1. if\n";
    cout << "  2. switch\n";
    cout << "  3. for\n";
    cout << "  4. while\n";
    cout << "  5. do-while\n";
    cout << "  6. break\n";
    cout << "  7. continue\n";
    cout << "  8. goto\n";
    cout << " Выберите вариант справки (q для выхода): ";
 
    cin >> choice;
  } while(choice < '1' || choice > '8' && choice != 'q');

  if(choice == 'q') break;

  switch(choice) {
    case '1':
      cout << "Инструкция if:\n\n";
      cout << "if(условие) иснтрукция;\n";
      cout << "else инструкция\n";
      break;
    case '2':
      cout << "Инструкция switch:\n\n";
      cout << "switch(выражение) {\n";
      cout << "   case константа:\n";
      cout << "      последовательность инструкций\n";
      cout << "      break;\n";
      cout << "   // ...\n";
      break;
    case '3':
      cout << "Инструкция for:\n\n";
      cout << "for(инициализация; условие; инкремент)";
      cout << " инструкция;\n";
    case '4':
      cout << "Инструкция while:\n\n";
      cout << "while(условие) инструкция;\n";
      break;
    case '5':
      cout << "Инструкция do-while:\n\n";
      cout << "do {\n";
      cout << " инструкция;\n";
      cout << "} while (условие);\n";
    case '6':
      cout << "Инструкция break:\n\n";
      cout << "break;\n";
      break;
    case '7':
      cout << "Инструкция continue:\n\n";
      cout << "continue;\n";
      break;
    case '8':
      cout << "Инструкция goto:\n\n";
      cout << "goto метка;\n";
  }
  cout << "\nНа этом все?(y/N) ";
  cin >> quit;

  if(quit == 'y' || quit == 'Y') {
    break;
  }
  else {
    continue;  
  }
}
  return 0;
}
