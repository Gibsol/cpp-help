#include <iostream>
using namespace std;

int main() {

  char choice;
  char quit;

  for(;;) {
  do {
    cout << "  1. if\n";
    cout << "  2. switch\n";
    cout << "  3. for\n";
    cout << "  4. while\n";
    cout << "  5. do-while\n";
    cout << "  6. break\n";
    cout << "  7. continue\n";
    cout << "  8. goto\n";
    cout << " Select a help option (q to quit): ";
 
    cin >> choice;
  } while(choice < '1' || choice > '8' && choice != 'q');

  if(choice == 'q') break;

  switch(choice) {
    case '1':
      cout << "Manual if:\n\n";
      cout << "if(condition) instruction;\n";
      cout << "else instruction\n";
      break;
    case '2':
      cout << "Instruction switch:\n\n";
      cout << "switch(phrase) {\n";
      cout << "   case constant:\n";
      cout << "      instruction sequence\n";
      cout << "      break;\n";
      cout << "   // ...\n";
      break;
    case '3':
      cout << "Manual for:\n\n";
      cout << "for(initialization; condition; increment)";
      cout << " Instruction;\n";
    case '4':
      cout << "Manual while:\n\n";
      cout << "while(condition) instruction;\n";
      break;
    case '5':
      cout << "Manual do-while:\n\n";
      cout << "do {\n";
      cout << " instruction;\n";
      cout << "} while (condition);\n";
    case '6':
      cout << "Manual break:\n\n";
      cout << "break;\n";
      break;
    case '7':
      cout << "Manual continue:\n\n";
      cout << "continue;\n";
      break;
    case '8':
      cout << "Manual goto:\n\n";
      cout << "goto tag;\n";
  }
  cout << "\nLeaving alredy?(y/N) ";
  cin >> quit;

  if(quit == 'y' || quit == 'Y') {
    break;

  }
  else {
    continue;  
  }
}
  return 0;
}
